#!/usr/bin/perl
# --------------------------------------------------------------------
# Synopsis: multiline synopsis would go here
# --------------------------------------------------------------------
# Usage:
# --------------------------------------------------------------------
# Options:
# Examples:
# Files:
#   external tools invoked, config files
# Environment:
#   external env var dependencies
# Description:
# Logs:
# Error codes:
# Globals:
# Inputs:
# Outputs:
# --------------------------------------------------------------------

# ====================================================================
# RCS INFORMATION (do not touch fields bounded by $)
# --------------------------------------------------------------------
#     $Date: 2012/02/04 14:19:13 $   (GMT)
# $Revision: 1.29 $
#   $Author: rodmant $ (aka last changed by)
#   $Locker: rodmant $
#   $Source: /usr/local/7Rq/package/cur/main-2010.11.26/lib/sh/RCS/_29r_sh.m4.sed-src,v $
#      $Log: _29r_sh.m4.sed-src,v $
# --------------------------------------------------------------------

# ====================================================================
# Copyright (c) 2013, 2016 Tom Rodman <Rodman.T.S@gmail.com>
# --------------------------------------------------------------------

package Sh::_sh;
use warnings;
use strict;
use Data::Dumper;

BEGIN {
  use Exporter   ();
  our ($VERSION, @ISA, @EXPORT, @EXPORT_OK, %EXPORT_TAGS);

  # set the version for version checking
  $VERSION      = 2013.01;

  @ISA      = qw(Exporter);
  @EXPORT     = qw(_sh _sh_long_stable);
  %EXPORT_TAGS = ( );     # eg: TAG => [ qw!name1 name2! ],

  # your exported package globals go here,
  # as well as any optionally exported functions
  # @EXPORT_OK   = qw($Var1 %Hashit &func3);
    @EXPORT_OK   = ();
}

sub _sh_long_stable {

    my $cmd = shift (@_);
    my $FUNCNAME = (caller(0))[3];
      # name of this subroutine

    my $_sh_STDOUT = `$cmd`;
      # let STDERR go to STDERR
    my $_errno = $!;
    my $shell_retval  = $? >> 8;
    chomp $_sh_STDOUT;

    if ($shell_retval)
    {
      print STDERR
        "\n$FUNCNAME:ERROR:when running:\n  [$cmd]:\n";
      print "    out: [$_sh_STDOUT]\n";
      print STDERR
        "    exit code: [$shell_retval]\n";
      print STDERR "errno: $_errno\n" if ($_errno);

      return ($shell_retval,$_sh_STDOUT);
    }
    else
    {
      return (0,$_sh_STDOUT);
        # Breaking the rule for perl truth, 0 for us means shell
        # command ran ok, ie returned 0.
    }
}

sub _sh {

    my $cmd = shift (@_);
    my $FUNCNAME = (caller(0))[3];
      # name of this subroutine

    my $eval_retval;

    my $_sh_STDOUT;
    my $_errno;
    my $child_err;

    $eval_retval =
        eval {
            # Credit to core module Sys::Hostname.
            # See also http://stackoverflow.com/questions/8389400/whats-the-difference-between-various-sigchld-values
            # TODO/FIXME: Explain need for this in more detail.  Curently I can not justify nor explain it, but I believe it is good.
                local $SIG{__DIE__};
                local $SIG{CHLD};

            $_sh_STDOUT = `$cmd`;
                # let STDERR go to STDERR
                # perl apparently does an exec, so parent shell is probably gone.  If command is not found,
                # retval in $? is *not* from shell, and it is a literal "-1".  
                # The shell always returns a positive value 0-255.  
                # See http://gnu-bash.2382.n7.nabble.com/bug-return-doesn-t-accept-negative-numbers-td2031i20.html
                # ex
                #     $ (exit -2);echo $?
                #     254

            $_errno = $!;
            $child_err  = $?;
                # Consider refactoring to use ${^CHILD_ERROR_NATIVE}.
        };

    $_sh_STDOUT = q{}
        unless(defined($_sh_STDOUT));

    chomp $_sh_STDOUT if($_sh_STDOUT);

    my $retval;
    if (   $child_err
        || ! defined( $eval_retval )
        || $@ )
    {
        ## Trouble!
        print STDERR Data::Dumper->Dump([$@], [qw(@)]) if ($@);

        print STDERR
            "\n$FUNCNAME:ERROR:when running:\n  [$cmd]:\n";

        print STDERR "    stdout: [$_sh_STDOUT]\n";

        my $tmp = Data::Dumper->Dump([$child_err], [qw(child_err)]);
        chomp ($tmp);
        print STDERR ""
            . $tmp
            . " ( perl's \$? )\n";

        if ( $child_err < 0 ) {

            $retval = $child_err;

            if ($retval == -1) {
                print STDERR qq{Testing suggests -1 => equivalent shell retval of 127/"command not found".\n};
            }
        }
        else {
            $retval = ( $child_err >> 8 );
            printf STDERR "exit status: [%d]\n", $retval;
        }

        print STDERR "errno: $_errno\n" if ($_errno);

        return ($retval, $_sh_STDOUT);
    }
    else
    {
        # Happy case.
        return (0, $_sh_STDOUT);
            # Breaking the rule for perl truth, 0 for us means shell
            # command ran ok, ie returned 0.
    }
}

=pod

ex:  Run long in job background and get bg PID:

    $ perl -e 'use Sh::_sh; ($retval, $out) = _sh(q{sleep 1234 >/dev/null 2>&1 & echo $!}); print "r: $retval o: $out\n";'
    r: 0 o: 31649

ex: Foreground job: ps of self:

    $ perl -e 'use Sh::_sh; ($retval, $out) = _sh(qq{ps -wwH -o pid,ppid,pgid,cputime,nice,user,tty,state,etime,bsdstart,nlwp,vsize,args -p $$}); print "r: $retval o: \n$out\n";'
    r: 0 o: 
      PID  PPID  PGID     TIME  NI USER     TT       S     ELAPSED  START NLWP    VSZ COMMAND
    32097  1343 32097 00:00:00   0 rodmant  pts/3    S       00:00  17:00    1   8832 perl -e use Sh::_sh; ($retval, $out) = _sh(qq{ps -wwH -o pid,ppid,pgid,cputime,nice,user,tty,state,etime,bsdstart,nlwp,vsize,args -p $$}); print "r: $retval o: \n$out\n";

=cut

1;

=head1 AUTHOR

  Tom Rodman <Rodman.T.S@gmail.com>

=head1 COPYRIGHT

  Copyright (c) 2013, 2016 Tom Rodman <Rodman.T.S@gmail.com>

=cut

