#!/usr/bin/perl
# --------------------------------------------------------------------
# Synopsis: multiline synopsis would go here
# --------------------------------------------------------------------
# Usage:
# --------------------------------------------------------------------
# Options:
# Examples:
# Files:
#   external tools invoked, config files
# Environment:
#   external env var dependencies
# Description:
# Logs:
# Error codes:
# Globals:
# Inputs:
# Outputs:
# --------------------------------------------------------------------

# ====================================================================
# RCS INFORMATION (do not touch fields bounded by $)
# --------------------------------------------------------------------
#     $Date: 2013/10/16 13:15:13 $   (GMT)
# $Revision: 1.2 $
#   $Author: rodmant $ (aka last changed by)
#   $Locker:  $
#   $Source: /usr/local/7Rq/package/cur/perl5.priv-2013.10.14/lib/perl5/File/RCS/_basename_follow_symlink.pm._m4,v $
#      $Log: _basename_follow_symlink.pm._m4,v $
#      Revision 1.2  2013/10/16 13:15:13  rodmant
#      *** empty log message ***
#
# --------------------------------------------------------------------

# ====================================================================
# Copyright (c) 2013 Tom Rodman <Rodman.T.S@gmail.com>
# --------------------------------------------------------------------

package File::_basename_follow_symlink;
use warnings;
use strict;

use Sh::_sh;

BEGIN {
  use Exporter   ();
  our ($VERSION, @ISA, @EXPORT, @EXPORT_OK, %EXPORT_TAGS);

  # set the version for version checking
  $VERSION      = 2013.01;

  @ISA      = qw(Exporter);
  @EXPORT     = qw(&_basename_follow_symlink);
  %EXPORT_TAGS = ( );     # eg: TAG => [ qw!name1 name2! ],

  # your exported package globals go here,
  # as well as any optionally exported functions
  # @EXPORT_OK   = qw($Var1 %Hashit &func3);
    @EXPORT_OK   = ();
}

sub _basename_follow_symlink
{
  my $pathname = shift (@_);
  my $FUNCNAME = (caller(0))[3];
    # name of this subroutine

  my ($namei_retval,$namei_out);

  ($namei_retval,$namei_out) = _sh(qq{namei "$pathname"});

  if ( $namei_out =~ m{^\s+[-]\s([^\n]+)\Z}xms )
  {
    return $1;
  }
  else
  {
    my $errout;
    ($errout=$namei_out) =~ s~^~  ~msg;
    print STDERR "$FUNCNAME:ERROR:\n";
    print STDERR "$errout\n";
    return 0;
  }
}

1;

=head1 AUTHOR

  Tom Rodman <Rodman.T.S@gmail.com>

=head1 COPYRIGHT

  Copyright (c) 2013 Tom Rodman <Rodman.T.S@gmail.com>

=cut

