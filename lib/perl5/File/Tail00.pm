#!/usr/bin/perl
# --------------------------------------------------------------------
# Synopsis: multiline synopsis would go here
# --------------------------------------------------------------------
# Usage:
# --------------------------------------------------------------------
# Options:
# Examples:
# Files:
#   external tools invoked, config files
# Environment:
#   external env var dependencies
# Description:
# Logs:
# Error codes:
# Globals:
# Inputs:
# Outputs:
# --------------------------------------------------------------------

# ====================================================================
# RCS INFORMATION (do not touch fields bounded by $)
# --------------------------------------------------------------------
#     $Date: 2012/02/04 14:19:13 $   (GMT)
# $Revision: 1.29 $
#   $Author: rodmant $ (aka last changed by)
#   $Locker: rodmant $
#   $Source: /usr/local/7Rq/package/cur/main-2010.11.26/lib/sh/RCS/_29r_sh.m4.sed-src,v $
#      $Log: _29r_sh.m4.sed-src,v $
# --------------------------------------------------------------------

# ====================================================================
# Copyright (c) 2013 Tom Rodman <Rodman.T.S@gmail.com>
# --------------------------------------------------------------------

package File::Tail00; # assumes File/Tail00.pm
use warnings;
use strict;
BEGIN {
  use Exporter   ();
  our ($VERSION, @ISA, @EXPORT, @EXPORT_OK, %EXPORT_TAGS);

  # set the version for version checking
  $VERSION	    = 0.05;

  @ISA	    = qw(Exporter);
  @EXPORT	    = qw(&_tail &_head);
  %EXPORT_TAGS = ( );     # eg: TAG => [ qw!name1 name2! ],

  # your exported package globals go here,
  # as well as any optionally exported functions
  # @EXPORT_OK   = qw($Var1 %Hashit &func3);
    @EXPORT_OK   = ();
}

use Tie::File;
use Fcntl 'O_RDONLY';

sub _tail {
  # credit: http://www.perlmonks.org/index.pl?node_id=162048

  my ($args) = @_;
  my @lines;
  tie @lines, 'Tie::File', $args->{filename}, mode => O_RDONLY
    or die "$!";
  return @lines[-$args->{linecount} .. -1];
}

sub _head {
  # credit: http://www.perlmonks.org/index.pl?node_id=162048

  my ($args) = @_;
  my @lines;
  tie @lines, 'Tie::File', $args->{filename}, mode => O_RDONLY
    or die "$!";
  return @lines[0 .. ( $args->{linecount} -1) ];
}

# ex:
  # PERL5LIB=pathname2thismod perl -e 'use File::Tail00; foreach my $x (_tail ({filename => "/etc/passwd", linecount => 5})){ print "$x\n"; } ;'

1;
