#!/usr/bin/perl

# --------------------------------------------------------------------
# Synopsis: Find min and max file timestamps and associated filenames
# in a single directory.
# --------------------------------------------------------------------
# Usage:
# --------------------------------------------------------------------
# Options:
# Examples:
# Files:
#   external tools invoked, config files
# Environment:
#   external env var dependencies
# Description:
# Logs:
# Error codes:
# Globals:
# Inputs:
# Outputs:
# --------------------------------------------------------------------
# Rating: tone: lib tool stable: n TBDs: y comments: my first OO perl
# --------------------------------------------------------------------

# ====================================================================
# RCS INFORMATION (do not touch fields bounded by $)
# --------------------------------------------------------------------
#     $Date: 2013/10/18 13:12:43 $   (GMT)
# $Revision: 1.9 $
#   $Author: rodmant $ (aka last changed by)
#   $Locker: rodmant $
#   $Source: /usr/local/7Rq/package/cur/perl5-2013.10.14/lib/perl5/File/RCS/dir_contents_age_range.pm._m4,v $
#      $Log: dir_contents_age_range.pm._m4,v $
#      Revision 1.9  2013/10/18 13:12:43  rodmant
#      *** empty log message ***
#
# --------------------------------------------------------------------

# ====================================================================
# Copyright (c) 2013 Tom Rodman <Rodman.T.S@gmail.com>
# --------------------------------------------------------------------

# -- Software License --
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ---

package File::dir_contents_age_range; 
use warnings;
use strict;
use File::Find ();

# This script was 'seeded' by 'find2perl'

# for the convenience of &wanted calls, including -eval statements:
use vars qw/*name *dir *prune/;
*name   = *File::Find::name;
*dir    = *File::Find::dir;
*prune  = *File::Find::prune;

sub wanted;

sub new
{
  my $class = $_[0];
  my $self = {
    topdir => $_[1],
    min => { name => undef, age => undef },
    min => { name => undef, age_human => undef },

    max => { name => undef, age => undef },
    max => { name => undef, age_human => undef },
  };
  bless $self, $class;
  return $self;
}

our $age_max;
our $age_min;
our $name_max;
our $name_min;

sub get_range
{
  my $self = $_[0];
  # Traverse desired filesystems
  
  File::Find::find({wanted => \&wanted}, $self->{topdir} );

  #print "min undef\n" unless (defined $age_min);
  #print "max undef\n" unless (defined $age_max);
  #print STDERR $age_min, $age_max;
  
  $self->{min}{name} = $name_min;
  $self->{min}{age} = $age_min;
  $self->{min}{age_human} = age_human($age_min);
  
  $self->{max}{name} = $name_max;
  $self->{max}{age} = $age_max;
  $self->{max}{age_human} = age_human($age_max);
  
}

#TBD: chg name of this sub
sub age_human
{
  my $epoch = shift;
  my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst);

  ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst)=gmtime($epoch);
  return sprintf "%04d%02d%02d_%02d%02d%02dGMT", $year-100+2000,$mon+1,$mday,$hour,$min,$sec;
}

sub age_range_human
{
  my $self = $_[0];
  return
    sprintf "%s,%s", $self->{min}{age_human}, $self->{max}{age_human};
}

sub wanted {
  my ($dev,$ino,$mode,$nlink,$uid,$gid);

  #my $age = -M _;

  my @_stat;

  if ( 
       ( @_stat = lstat($_) ) 
       && 
       -f _ 
     )
  {

    my $age = $_stat[9];
    
    if (! defined $age_max)
    {
       $age_max = $age;
       $age_min = $age;
       $name_max= $name;
       $name_min= $name;
       return;
    }

    #print "$name\n";

    if ($age > $age_max)
    {
      $age_max = $age;
      $name_max= $name;
    }

    if ($age < $age_min)
    {
      $age_min = $age;
      $name_min= $name;
    }
  }
}

1;

__END__

#TBD update this example, it is out of date
#ex
  $ true;(cd  /usr/local/perlhost_qmp5/lib/perl5/File/;perl -e 'use dir_contents_age_range; my $zap = File::dir_contents_age_range->new("/u/rodmant/db"); print $zap->{topdir} . "\n\n";$zap->get_range;printf "%s\n%s\n%s\n%s\n", $zap->{max}{name},$zap->{max}{age_human},$zap->{min}{name},$zap->{min}{age_human};' ) ##
  /u/rodmant/db

  /u/rodmant/db
  20131014_131543GMT

  /u/rodmant/db/office
  19920917_182128GMT

